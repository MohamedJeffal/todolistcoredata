//
//  ToDoItemu.m
//  ToDoListCoreData
//
//  Created by Guest User on 10/12/15.
//  Copyright (c) 2015 jeffal_m. All rights reserved.
//

#import "ToDoItemu.h"

@interface ToDoItemu ()
@property NSDate *completionDate;
@end

@implementation ToDoItemu

@dynamic itemName;
@dynamic completed;
@dynamic creationDate;

- (void)markAsCompleted:(BOOL)isComplete {
    self.completed = isComplete;
    [self setCompletionDate];
}
- (void)setCompletionDate {
    if (self.completed) {
        self.completionDate = [NSDate date];
    } else {
        self.completionDate = nil;
    }
}

@end
