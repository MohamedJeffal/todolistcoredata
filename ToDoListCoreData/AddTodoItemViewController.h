//
//  AddTodoItemViewController.h
//  ToDoListCoreData
//
//  Created by Guest User on 10/12/15.
//  Copyright (c) 2015 jeffal_m. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToDoItem.h"

@interface AddTodoItemViewController : UIViewController

@property ToDoItem *toDoItem;

@end
