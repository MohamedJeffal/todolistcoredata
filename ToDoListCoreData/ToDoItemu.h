//
//  ToDoItemu.h
//  ToDoListCoreData
//
//  Created by Guest User on 10/12/15.
//  Copyright (c) 2015 jeffal_m. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ToDoItemu : NSManagedObject

@property (nonatomic, retain) NSString * itemName;
@property (nonatomic, retain) NSNumber * completed;
@property (nonatomic, retain) NSDate * creationDate;

- (void)markAsCompleted:(BOOL)isComplete;

@end
